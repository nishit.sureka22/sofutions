<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\SampleTest;

class UnitTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testExample()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSee('Laravel');
        $response->assertDontSee('User');
    }

    public function testBoxContents()
    {
        $box = new SampleTest(['toy']);
        $this->assertTrue($box->has('toy'));
        $this->assertFalse($box->has('ball'));
    }

    public function testTakeOneFromTheBox()
    {
        $box = new SampleTest(['torch']);
        $this->assertEquals('torch', $box->takeOne());

        $this->assertNull($box->takeOne());
    }

    public function testStartsWithALetter()
    {
        $box = new SampleTest(['toy', 'torch', 'ball', 'cat', 'tissue']);
        $results = $box->startsWith('t');
        $this->assertCount(3, $results);
        $this->assertContains('toy', $results);
        $this->assertContains('torch', $results);
        $this->assertContains('tissue', $results);

        // Empty array if passed even
        $this->assertEmpty($box->startsWith('s'));
    }
}
