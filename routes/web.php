<?php
  
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
  
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  
Route::get('/', function () {
    return view('welcome');
});
  
Auth::routes();
Auth::routes(['register' => false]);
  
/*-----------Users Routes List---------------*/
Route::middleware(['auth', 'user-access:user'])->group(function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
});

/*-----------Admin Routes List--------------*/
Route::middleware(['auth', 'user-access:admin'])->group(function () {
    Route::get('/admin/home', [HomeController::class, 'adminHome'])->name('admin.home');

    //companies route
    Route::get('/admin/companies/index', [CompanyController::class, 'index'])->name('companies.index');
    Route::get('/admin/companies/create', [CompanyController::class, 'create'])->name('companies.create');
    Route::post('/admin/companies/create', [CompanyController::class, 'store'])->name('companies.store');
    Route::get('/admin/companies/edit/{id}', [CompanyController::class, 'edit'])->name('companies.edit');
    Route::post('/admin/companies/edit/{id}', [CompanyController::class, 'update'])->name('companies.update');
    Route::get('/admin/companies/{id}', [CompanyController::class, 'destroy'])->name('companies.delete');

    //employees route
    Route::get('/admin/employees/index', [EmployeeController::class, 'index'])->name('employees.index');
    Route::get('/admin/employees/create', [EmployeeController::class, 'create'])->name('employees.create');
    Route::post('/admin/employees/create', [EmployeeController::class, 'store'])->name('employees.store');
    Route::get('/admin/employees/edit/{id}', [EmployeeController::class, 'edit'])->name('employees.edit');
    Route::post('/admin/employees/edit/{id}', [EmployeeController::class, 'update'])->name('employees.update');
    Route::get('/admin/employees/{id}', [EmployeeController::class, 'destroy'])->name('employees.delete');
});