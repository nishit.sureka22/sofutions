<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <link rel="stylesheet" href="{{URL::asset('plugins/fontawesome-free/css/all.min.css')}}">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="{{URL::asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('plugins/jqvmap/jqvmap.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('dist/css/adminlte.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('plugins/daterangepicker/daterangepicker.css')}}">
        <link rel="stylesheet" href="{{URL::asset('plugins/summernote/summernote-bs4.min.css')}}">

        <!-- @vite(['resources/sass/app.scss', 'resources/js/app.js']) -->
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTELogo" height="60" width="60">
            </div>

            @include('admin.partials.header')
            @include('admin.partials.sidebar')

            <div class="content-wrapper">
                @include('admin.partials.breadcrumb')

                <section class="content">
                    @yield('content')
                </section>
            
            </div>
            @include('admin.partials.footer')
        
            <aside class="control-sidebar control-sidebar-dark"></aside>
        </div>

        <script src="{{URL::asset('plugins/jquery/jquery.min.js')}}"></script>
        <script src="{{URL::asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
        <script>
        $.widget.bridge('uibutton', $.ui.button)
        </script>
        <script src="{{URL::asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{URL::asset('plugins/chart.js/Chart.min.js')}}"></script>
        <script src="{{URL::asset('plugins/sparklines/sparkline.js')}}"></script>
        <script src="{{URL::asset('plugins/jqvmap/jquery.vmap.min.js')}}"></script>
        <script src="{{URL::asset('plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
        <script src="{{URL::asset('plugins/jquery-knob/jquery.knob.min.js')}}"></script>
        <script src="{{URL::asset('plugins/moment/moment.min.js')}}"></script>
        <script src="{{URL::asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
        <script src="{{URL::asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
        <script src="{{URL::asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
        <script src="{{URL::asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
        <script src="{{URL::asset('dist/js/adminlte.js')}}"></script>
        <script src="{{URL::asset('dist/js/demo.js')}}"></script>
        <script src="{{URL::asset('dist/js/pages/dashboard.js')}}"></script>
        <script src="{{URL::asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
        <script>
        $(function () {
        bsCustomFileInput.init();
        });
        </script>
        <script>
        function myFunction() {
            if(!confirm("Are You Sure to delete this"))
            event.preventDefault();
        }
        </script>
    </body>
</html>
