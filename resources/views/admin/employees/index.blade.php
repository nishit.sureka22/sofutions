@extends('admin.layouts.app')
  
@section('content')
<div class="card">
        <div class="card-header">
          <h3 class="card-title">All employees</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
              <a class="btn btn-primary btn-sm" href="{{route('employees.create')}}">
                <i class="fas fa-folder">
                </i>
                Create
            </a>
          </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          First Name
                      </th>
                      <th style="width: 30%">
                          Last Name
                      </th>
                      <th>
                          Company
                      </th>
                      <th style="width: 8%" class="text-center">
                          Email
                      </th>
                      <th style="width: 8%" class="text-center">
                          Phone
                      </th>
                      <th style="width: 20%" class="text-center">
                         Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    @foreach($employees as $key => $empObj)
                      <td>
                          {{++$key}}
                      </td>
                      <td> {{$empObj->first_name}} </td>
                      <td> {{$empObj->last_name}} </td>
                      <td> {{$empObj->companies->name}} </td>
                      <td> {{$empObj->email}} </td>
                      <td> {{$empObj->phone}} </td>
                      <td class="project-actions text-right">

                          <a class="btn btn-info btn-sm" href="{{route('employees.edit', $empObj->id)}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm" onclick="return myFunction();" href="{{route('employees.delete', $empObj->id)}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
          <div class="p-5 d-flex justify-content-center">
            {!! $employees->links() !!}
        </div>
        </div>
    </div>
@endsection
