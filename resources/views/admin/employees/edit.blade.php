@extends('admin.layouts.app')  
@section('content')  
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Create employees</h3>
                </div>
                
                <form id="companyForm" method="post" action="{{ route('employees.update', $employees->id) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" name="first_name" class="form-control" value="{{ $employees->first_name }}" id="first_name" placeholder="Enter First Name">
                            @if ($errors->has('first_name'))
                                <span class="text-danger">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="last_name">First Name</label>
                            <input type="text" name="last_name" class="form-control" id="last_name" value="{{ $employees->last_name }}" placeholder="Enter Last Name">
                            @if ($errors->has('last_name'))
                                <span class="text-danger">{{ $errors->first('last_name') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email" value="{{ $employees->email }}" placeholder="Enter email">
                            @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="company">Company</label>
                            <select class="form-control" id="company" name="company">
                                @foreach($companies as $company)
                                    <option value='{{ $company->id}}' {{($employees->company === $company->id) ? 'Selected' : ''}}> {{ $company->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('company'))
                                <span class="text-danger">{{ $errors->first('company') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="tel" id="phone" class="form-control" name="phone" value="{{ $employees->phone }}" pattern="[0-9]{10}">
                            @if ($errors->has('phone'))
                                <span class="text-danger">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>
</div>  
@endsection  