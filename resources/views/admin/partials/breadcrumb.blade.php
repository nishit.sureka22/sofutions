<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{$heading}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">{{$page_title}}</a></li>
                    <li class="breadcrumb-item active">{{$sub_page_title}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>