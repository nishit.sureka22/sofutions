@extends('admin.layouts.app')
  
@section('content')
<div class="card">
        <div class="card-header">
          <h3 class="card-title">All Companies</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
              <a class="btn btn-primary btn-sm" href="{{route('companies.create')}}">
                <i class="fas fa-folder">
                </i>
                Create
            </a>
          </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 20%">
                          Name
                      </th>
                      <th style="width: 30%">
                          Logo
                      </th>
                      <th>
                          Email
                      </th>
                      <th style="width: 8%" class="text-center">
                          Website
                      </th>
                      <th style="width: 20%" class="text-center">
                         Action
                      </th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                    @foreach($companies as $key => $comObj)
                      <td>
                          {{++$key}}
                      </td>
                      <td> {{$comObj->name}} </td>
                      <td>
                          <ul class="list-inline">
                              <li class="list-inline-item">
                                  <img alt="Avatar" class="table-avatar" src="{{Storage::url($comObj->logo)}}">
                              </li>
                          </ul>
                      </td>

                      <td> {{$comObj->email}} </td>
                      <td> {{$comObj->website}} </td>
                      <td class="project-actions text-right">

                          <a class="btn btn-info btn-sm" href="{{route('companies.edit', $comObj->id)}}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm" onclick="return myFunction();" href="{{route('companies.delete', $comObj->id)}}">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
          <div class="p-5 d-flex justify-content-center">
            {!! $companies->links() !!}
        </div>
        </div>
    </div>
@endsection
