<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Companies;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $heading = 'Companies';
        $page_title = 'Home';
        $sub_page_title = 'Companies';

        $companies = Companies::Paginate(10);
        return view('admin.companies.index',compact('page_title','sub_page_title','heading','companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $heading = 'Companies';
        $page_title = 'Home';
        $sub_page_title = 'Create Companies';
        return view('admin.companies.create',compact('page_title','sub_page_title','heading'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
  
        $rules=array(
            'name' => 'required',
            'image.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|',
        );
        $messages=array(
            'name.required' => 'Please enter company name.',
            'image.required' => 'minimum 100X100 Pixel',
        );
        $validator=Validator::make($request->all(),$rules,$messages);
        if($validator->fails())
        {
            $messages=$validator->messages();
            return response()->json(["messages"=>$messages], 500);
        }
        
        try {
            \DB::beginTransaction();
            
            $companies = new Companies();
            if($file = $request->hasFile('logo')) {
                $file = $request->file('logo') ;
                // $contents = Storage::get($file);
                // $size = Storage::size($file);
                // $unix_timestamp = Storage::lastModified($file);
                $filename = Storage::putFile(
                    'public',
                    $file);
                $companies->logo          =  $filename;
            }
           
            $companies->name        =  $request->get('name');
            $companies->email        =  $request->get('email');
            $companies->website  =  $request->get('website');
            $companies->save();
        
            \DB::commit();
        
        } catch (Throwable $e) {
            \DB::rollback();
        }  

        return redirect()->route('companies.index')->with('success','Company added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $heading = 'Companies';
        $page_title = 'Home';
        $sub_page_title = 'Update Companies';

        $companies = Companies::findOrFail($id);
        return view('admin.companies.edit', compact('companies','page_title','sub_page_title','heading'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=array(
            'name' => 'required',
            'image.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|',
        );
        $messages=array(
            'name.required' => 'Please enter company name.',
            'image.required' => 'minimum 100X100 Pixel',
        );
        $validator=Validator::make($request->all(),$rules,$messages);
        if($validator->fails())
        {
            $messages=$validator->messages();
            return response()->json(["messages"=>$messages], 500);
        }
        
        try {
            \DB::beginTransaction();
            
            $companies = Companies::where('id',$request->id)->first();
            if($file = $request->hasFile('logo')) {
                $file = $request->file('logo') ;
                $filename = Storage::putFile(
                    'public',
                    $file);
                $companies->logo  =  $filename;
            }
           
            $companies->name        =  $request->get('name');
            $companies->email        =  $request->get('email');
            $companies->website  =  $request->get('website');
            $companies->save();
        
            \DB::commit();
        
        } catch (Throwable $e) {
            \DB::rollback();
        }  

        return redirect()->route('companies.index')->with('success','Company updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $companies = Companies::findOrFail($id);
        $companies->delete();
        return redirect()->route('companies.index')->with('danger', 'Company has been deleted');
    }
}
