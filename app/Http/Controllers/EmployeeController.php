<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Employees;
use App\Models\Companies;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $heading = 'Employees';
        $page_title = 'Home';
        $sub_page_title = 'Employees';
        $employees = Employees::with('companies')->paginate(10);

        return view('admin.employees.index',compact('page_title','sub_page_title','heading','employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $heading = 'Employees';
        $page_title = 'Home';
        $sub_page_title = 'Create Employees';

        $companies = Companies::all();
        return view('admin.employees.create',compact('page_title','sub_page_title','heading','companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $rules=array(
            'first_name' => 'required',
            'last_name' => 'required',
        );
        $messages=array(
            'first_name.required' => 'Please enter first name.',
            'last_name.required' => 'Please enter last name.',
        );
        $validator=Validator::make($request->all(),$rules,$messages);
        if($validator->fails())
        {
            $messages=$validator->messages();
            return response()->json(["messages"=>$messages], 500);
        }
        
        try {
            \DB::beginTransaction();
            
            $employees = new employees();
            $employees->fill($request->all());
            $employees->save();
        
            \DB::commit();
        
        } catch (Throwable $e) {
            \DB::rollback();
        }  

        return redirect()->route('employees.index')->with('success','Employee added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $heading = 'Employees';
        $page_title = 'Home';
        $sub_page_title = 'Update Employees';

        $employees = Employees::findOrFail($id);
        $companies = Companies::all();
        return view('admin.employees.edit', compact('employees','page_title','sub_page_title','heading','companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=array(
            'first_name' => 'required',
            'last_name' => 'required',
        );
        $messages=array(
            'first_name.required' => 'Please enter first name.',
            'last_name.required' => 'Please enter last name.',
        );
        $validator=Validator::make($request->all(),$rules,$messages);
        if($validator->fails())
        {
            $messages=$validator->messages();
            return response()->json(["messages"=>$messages], 500);
        }
        
        try {
            \DB::beginTransaction();
            
            $employees = Employees::where('id',$request->id)->first();           
            $employees->fill($request->all());
            $employees->save();
        
            \DB::commit();
        
        } catch (Throwable $e) {
            \DB::rollback();
        }  

        return redirect()->route('employees.index')->with('success','Employee updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employees = Employees::findOrFail($id);
        $employees->delete();
        return redirect()->route('employees.index')->with('danger', 'employee has been deleted');
    }
}
